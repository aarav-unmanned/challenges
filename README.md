# Want to join us?

We at AUS are looking for talented engineers to join our team.

We think that a coding challenge gives the candidate a chance to demonstrate:
- Coding skills
- How you think
- How you solve a problem

You can access the challenges in different branches.
